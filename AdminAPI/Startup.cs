using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspNetCore.Serilog.RequestLoggingMiddleware;
using AdminAPI.Configurations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace AdminAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //api Db set up
            services.AddDatabaseConfig(Configuration);

            //DI settings
            services.AddDependencyInjectionsConfig();

            //authentification
            services.AddAuthConfig();

            //http clients configuration
            services.AddHttpClientsConfig(Configuration);

            //CORS settings
            services.AddCorsConfig(Configuration);

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            //cookies
            services.AddCookiesConfig();

            //SignalR
            services.AddSignalRConfig();

            //swagger
            services.AddSwaggerConfig();

            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseAuthentication();
            app.UseRouting();
            app.UseAuthorization();

            app.UseCors();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
                //endpoints.MapHub<AdminHub>("/hubs/adminHub");
            });

            app.UseSwagger();

            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "API");
            });
            //app.UseSerilogRequestLogging();
        }
    }
}
