﻿using AdminAPI.Entities;
using AdminAPI.Repository.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminAPI.Policies
{
    public class BanPolicy : AuthorizationHandler<BanRequirement>, IAuthorizationRequirement
    {
        private readonly UnitOfWork _unitOfWork;

        public BanPolicy(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, BanRequirement requirement)
        {
            User user = _unitOfWork.UsersRepository.DbSet
                .Where(u => u.UserName.Equals(context.User.Identity.Name))
                .FirstOrDefault();

            if (user != default)
            {
                if (user.LockoutEnd == null || user.LockoutEnd < DateTime.UtcNow)
                {
                    context.Succeed(requirement);
                }
            }

            return Task.CompletedTask;
        }
    }
}
