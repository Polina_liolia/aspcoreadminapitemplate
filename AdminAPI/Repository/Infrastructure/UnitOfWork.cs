﻿using AdminAPI.Entities;

namespace AdminAPI.Repository.Infrastructure
{
    public class UnitOfWork
    {
        public GyDbContext Context { get; }

        public UnitOfWork(GyDbContext context)
        {
            Context = context;
        }

        //user and player
        private BaseRepository<User, GyDbContext> _usersRepository;
        public BaseRepository<User, GyDbContext> UsersRepository
        {
            get
            {
                if (_usersRepository == null)
                {
                    _usersRepository = new BaseRepository<User, GyDbContext>(Context);
                }
                return _usersRepository;
            }
        }
    }
}
