﻿using AdminAPI.Entities.BaseEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AdminAPI.Repository.Infrastructure
{
    public class BaseRepository<T, TContext> where T : class, IEntity where TContext : DbContext
    {
        protected TContext _dbContext;
        protected IServiceProvider _sp;
        protected IQueryable<T> _relations;

        public BaseRepository(TContext context)
        {
            _dbContext = context;
            DbSet = _dbContext.Set<T>();
            _relations = DbSet;
        }

        public DbSet<T> DbSet { get; }

        public void Add(T entity)
        {
            entity.CreatedAt = DateTime.UtcNow;
            entity.ModifiedAt = DateTime.UtcNow;
            _dbContext.Add(entity);
        }

        public void Update(T entity)
        {
            entity.ModifiedAt = DateTime.UtcNow;
            DbSet.Update(entity);
        }

        public virtual T Get(string id, bool limitInclude = false)
        {
            IQueryable<T> querySource = limitInclude ? DbSet : _relations;
            return querySource.FirstOrDefault(t => t.Id == id);
        }

        public virtual T Get(Expression<Func<T, bool>> predicate, bool limitInclude = false)
        {
            IQueryable<T> querySource = limitInclude ? DbSet : _relations;
            return querySource.FirstOrDefault(predicate);
        }

        public virtual IList<T> GetAll(bool limitInclude = false)
        {
            IQueryable<T> querySource = limitInclude ? DbSet : _relations;
            return querySource.ToList();
        }

        public virtual IList<T> GetAll(Expression<Func<T, bool>> predicate, bool limitInclude = false)
        {
            IQueryable<T> querySource = limitInclude ? DbSet : _relations;
            return querySource.Where(predicate)
                .ToList();
        }

        public bool Delete(string id)
        {
            bool removed = false;
            T entity = Get(id);
            if (entity != null)
            {
                DbSet.Attach(entity);
                DbSet.Remove(entity);
                removed = true;
            }
            return removed;
        }

        public async Task<bool> Delete(Expression<Func<T, bool>> predicate)
        {
            bool removed = false;
            List<T> entities = await DbSet.Where(predicate).ToListAsync();
            if (entities.Count > 0)
            {
                DbSet.RemoveRange(entities);
                removed = true;
            }
            return removed;
        }
    }
}
