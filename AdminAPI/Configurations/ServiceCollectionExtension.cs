﻿using AdminAPI.Entities;
using AdminAPI.Policies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Linq;
using Microsoft.Extensions.Configuration;
using System.Reflection;
using System.IO;
using Microsoft.EntityFrameworkCore;
using AdminAPI.Repository.Infrastructure;
using Serilog;

namespace AdminAPI.Configurations
{
    public static class ServiceCollectionExtension
    {
        public static void AddDatabaseConfig(this IServiceCollection services, IConfiguration configuration)
        {
            string apiDbConnectionString = configuration.GetValue<string>("DbConnectionString");

            services.AddDbContext<GyDbContext>(options =>
            {
                //options.UseSqlServer(apiDbConnectionString);
                options.UseNpgsql(apiDbConnectionString);
                options.EnableSensitiveDataLogging(true);
            });
        }

        public static void AddDependencyInjectionsConfig(this IServiceCollection services)
        {
            services.AddTransient<IAuthorizationHandler, BanPolicy>();
            services.AddScoped<UnitOfWork>();
        }

        public static void AddAuthConfig(this IServiceCollection services)
        {
            services.AddIdentity<User, IdentityRole>(options => options.SignIn.RequireConfirmedAccount = true)
                    .AddEntityFrameworkStores<GyDbContext>()
                    .AddDefaultTokenProviders();

            services.AddAuthentication(cfg =>
            {
                cfg.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                cfg.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                   .AddJwtBearer(options =>
                   {
                       options.RequireHttpsMetadata = false;
                       options.TokenValidationParameters = new TokenValidationParameters
                       {
                           ValidateIssuer = true,
                           ValidIssuer = AuthOptions.Issuer,
                           ValidateAudience = true,
                           ValidAudience = AuthOptions.Audience,
                           ValidateLifetime = true,
                           IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                           ValidateIssuerSigningKey = true,
                           ClockSkew = TimeSpan.Zero
                       };
                   });

            services.AddAuthorization(opts =>
            {
                opts.DefaultPolicy = new AuthorizationPolicyBuilder()
            .AddRequirements(new BanRequirement())
            .Build();
            });

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings.
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 1;

                // Lockout settings.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                // User settings.
                options.User.AllowedUserNameCharacters =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                options.User.RequireUniqueEmail = true;
            });
        }

        public static void AddHttpClientsConfig(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddControllersWithViews();
        }

        public static void AddCorsConfig(this IServiceCollection services, IConfiguration configuration)
        {
            string[] defaultHostsAllowed = configuration.GetSection("DefaultOrigins")?.GetChildren()?.Select(x => x.Value)?.ToArray();
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                    builder =>
                    {
                        builder.WithOrigins(defaultHostsAllowed)
                               .AllowCredentials()
                               .AllowAnyHeader()
                               .WithMethods("GET", "POST", "PUT", "DELETE");
                    });
            });
        }

        public static void AddCookiesConfig(this IServiceCollection services)
        {
        }

        public static void AddSwaggerConfig(this IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1",
                    new Microsoft.OpenApi.Models.OpenApiInfo
                    {
                        Title = "API",
                        Version = "v1"
                    });
                string fileName = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                string filePath = Path.Combine(AppContext.BaseDirectory, fileName);
                options.IncludeXmlComments(filePath);
            });
        }

        public static void AddSignalRConfig(this IServiceCollection services)
        {
            services.AddSignalR();
        }
    }
}
