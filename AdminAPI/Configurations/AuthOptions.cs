﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace AdminAPI.Configurations
{
    public static class AuthOptions
    {
        public static string Issuer { get; }
        public static string Audience { get; }
        public static string Key { get; }
        public static int TokenLifetime { get; }

        static AuthOptions()
        {
            Issuer = "GoodYearApi";
            Audience = "gy_users";
            Key = "JBFynyDM6GOQnQdai47C";
            TokenLifetime = 7 * 24 * 60;
        }

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));
        }
    }
}
